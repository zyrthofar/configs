# Installing Arch Linux on PC

# Network setup
systemctl stop dhcpcd@eno1.service
ip link show # wlp6s0 is down
ip link set wlp6s0 up
wpa_supplicant -B -i wlp6s0 -c <(wpa_passphrase "ssid" password)
ip address show # no ipv4 address
ip address add 192.168.0.112/24 broadcast + dev wlp6s0
ip route show # ping 192.168.0.112 works, ping 1.1.1.1 doesn't
ip route add 0.0.0.0/0 via 192.168.0.1 dev wlp6s0
# ping 1.1.1.1 works, ping archlinux.org doesn't
echo "nameserver 1.1.1.1" >> /etc/resolv.conf # cloudflare DNS ip addresses
echo "nameserver 1.0.0.1" >> /etc/resolv.conf
# ping archlinux.org works

# Disks and partitions setup
fdisk -l # shows partitions and sizes
lsblk -f # shows partitions and mountpoints
fdisk /dev/sda
  g # creates GPT (GUID Partition Table)
  n; 1; default; +550M # creates sda1 for boot, recommended 550Mib
  n; 2; default; +32G # creates sda2 for root, recommended 23-32Gib
  n; 3; default; +32G # creates sda3 for swap, recommended same as RAM
  n; 4; default; default # creates sda4 for home, rest of space
  t; 1; 1 # makes sda1 a EFI System
  t; 3; 19 # makes sda3 a Linux swap
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sda4
mkswap /dev/sda3
swapon /dev/sda3

# Installing Arch Linux
mkdir /mnt/boot && mkdir /mnt/home
mount /dev/sda2 /mnt
mount /dev/sda3 /mnt/boot
mount /dev/sda4 /mnt/home
pacstrap /mnt base
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Canada/Eastern /etc/localtime
hwclock --systohc
(uncomment wanted locales in /etc/locale.gen)
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf
echo "arch_main" > /etc/hostname # machine/host name
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1 localhost" >> /etc/hosts
echo "127.0.0.1 arch_main.localdomain arch_main" >> /etc/hosts
passwd
pacman -Sy grub efibootmgr intel-ucode wpa_supplicant
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub
grub-mkconfig -o /boot/grub/grub.cfg
reboot

# Post-installation setup
(re-setup network, copy profile from /etc/netctl/examples to /etc/netctl)
netctl start (profile)
netctl enable (profile) # to start it on boot
pacman -Sy sudo
visudo # uncomment line related to %sudo
groupadd sudo
useradd -m -G sudo simon
passwd simon
passwd -l root # this locks root -- verify that simon works first!

# Add following to ~/.bashrc
PS1="\$(EXIT="\$?"; if [ \$EXIT != 0 ]; then echo -e -n "\\033[31m\$EXIT"; echo -n ' \033[0m'; fi)\033[2m\u@\h \w\033[0m\n\$ "

# Installing applications
pacman -Sy xorg-server # display server, I chose the proprietary nvidia-340xx-utils instead of libglvnd
pacman -Sy xfce4 xfce4-goodies gvfs # desktop environment
pacman -Sy lightdm lightdm-gtk-greeter light-locker # display manager
systemctl enable ligntdm.service
pacman -Sy ttf-liberation

create /etc/fonts/local.conf with:
  <?xml version="1.0"?>
  <!DOCTYPE fontconfig SYSTEM "fonts.dtd">
  <fontconfig>
    <alias>
      <family>serif</family>
      <prefer><family>Liberation Serif</family></prefer>
    </alias>
    # Same with sans-serif -> Liberation Sans, and monospace -> Liberation Mono
  </fontconfig>

xfconf-query -c xfce4-session -p /general/LockCommand -s "light-locker-command --lock" --create -t string
(add "light-locker-command --lock && exit" to /usr/bin/xflock)

pacman -Sy firefox thunderbird keepassxc git
pacman -S --needed base-devel
mkdir ~/aur_builds && cd ~/aur_builds
git clone (dropbox git url in AUR) && cd dropbox
makepkg -si
cd




# Keep numlock on
pacman -Sy numlockx
set `greeter-setup-script=/usr/bin/numlockx on` under `[Seat:*]`



