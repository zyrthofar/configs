# This is intended to setup a temporary vm to test docker or kubernetes stuff.
# It is only protected by a user password, and has the firewall set to accept incoming ports 80 and 8080..
#
# Do NOT use it as a permanent vm, and remove/disable it when it is not in use.
#
# When this setup is used to create a digitalocean snapshop to create other droplets, the ssh password
# authentication will be disabled. The `PasswordAuthentication` will have to be reset to `yes`, and the
# ssh service restarted from the digitalocean console.

apt-get update && apt-get upgrade -y

# Creates a new user named `user`.
useradd -m -s /bin/bash user
passwd user
usermod -aG sudo user

# Installs a firewall, and only open ports 22, 80, and 8080.
apt install ufw
ufw allow ssh
ufw allow http
ufw allow 8080
ufw enable

# Modify ssh configuration.
vim /etc/ssh/sshd_config # Set `PermitRootLogin` to `no`, `PasswordAuthentication` to yes.
service ssh restart

# Test this new user using another terminal. It should be able to login and use `sudo`.
# When ready, logout from `root` and login with `user`.

# Disables root.
sudo passwd -l root

# Installs docker.
sudo apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
sudo apt-get update && sudo apt-get install -y docker-ce
sudo usermod -aG docker user # Group will be added next time `user` logins.

# Installs docker-compose.
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Installs kubectl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update && sudo apt-get install -y kubectl

# Autocompletion.
sudo curl -L https://raw.githubusercontent.com/docker/compose/1.24.1/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
kubectl completion bash >/etc/bash_completion.d/kubectl

# refs.
# https://www.digitalocean.com/community/tutorials/initial-server-setup-with-debian-10
# https://docs.docker.com/v17.12/install/linux/docker-ce/debian/
# https://docs.docker.com/compose/install/
# https://kubernetes.io/docs/tasks/tools/install-kubectl/
# https://docs.docker.com/compose/completion/
